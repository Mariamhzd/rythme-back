const express = require('express');
const concertController = require('../controllers/concert.controller');

const router = express.Router();

router.get("/", concertController.indexGet);

router.get("/:id", concertController.indexGetId);

router.post("/create", concertController.indexPost);

router.put("/add-artist-concertHall", concertController.indexPut);

module.exports = router;
const Style = require('../models/Style');

const indexGet = async (req, res) => {
    try {
        const style = await Style.find();

        return res.status(200).json(style)
    
    } catch (error) {
        return res.status(500).json(error);
    }
}

module.exports = { indexGet }
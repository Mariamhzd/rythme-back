const Ticket = require("../models/Ticket");

const indexGet = async(req, res, next) => {
    try {
        const tickets = await Ticket.find().populate('user')

        return res.status(200).json({ tickets });
    } catch (err) {
        next(err);
    }
}

const indexPost = async(req, res, next) => {
    try {

        const newticket = new Ticket({
            name: req.body.name,
            email: req.body.email,
            postelcode: req.body.postelcode,
            tickets: req.body.tickets,
            singer: req.body.singer,
            hall: req.body.hall,
            date: req.body.date,
            hour: req.body.hour,
            price: req.body.price,
            user: req.body.tickets.userId,
        });

        const createdTicket = await newticket.save();
        console.log('created',createdTicket)
        return res.status(200).json(createdTicket);
    } catch (err) {
        next(err);
    }
};

module.exports = { indexGet, indexPost };
const ConcerHall = require('../models/ConcertHall');


const indexGet = async (req, res) => {
    try {
        const concerthall = await ConcerHall.find();

        return res.status(200).json(concerthall)

    } catch (error) {
        return res.status(500).json(error);
    }    
}

module.exports = { indexGet };
const mongoose = require('mongoose');
const DB_URL = require('../db').DB_URL;
const Style = require('../models/Style');

const styles = [
    {
        name: "Soul",
        picture: "https://p14cdn4static.sharpschool.com/UserFiles/Servers/Server_3835114/Image/King/soul%20music.jpg",
    },
    {
        name: "Rock",
        picture:"https://image.freepik.com/vector-gratis/logotipo-musica-rock_1895-231.jpg",
    },
    {
        name: "Pop",
        picture: "https://pbs.twimg.com/profile_images/1155517544977108992/883LNqWD_400x400.jpg",
    },
    {
        name: "Jazz",
        picture:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxh6Vpn4OUtLT4u66VPs-_aYgOi6ZlZGM-bA&usqp=CAU",
    },
    {
        name: "Indie",
        picture: "https://thumbs.dreamstime.com/b/dise%C3%B1o-dise%C3%B1ado-vintage-de-la-insignia-m%C3%BAsica-rock-del-indie-plantilla-para-su-cartel-aviador-bandera-ilustraci%C3%B3n-vector-105311238.jpg",
    },
    {
        name: "Hip-Hop",
        picture: "https://en.pimg.jp/048/152/239/1/48152239.jpg",
    },
    {
        name: "House",
        picture: "https://i.ebayimg.com/images/g/OhQAAOSw5XJcsgz-/s-l400.jpg",
    },
]

mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true, 
 })
 .then(async () => {
     const allStyle = await Style.find();

     if(allStyle.length) {
         await Style.collection.drop();
     }
 })
 .catch((err) => {
    console.log(`Error deleting db data ${err}`);
})
 .then(async () => {
     await Style.insertMany(styles);
 })
 .catch((err) => {
    console.log(`Error adding data to our db ${err}`)
})
.finally(() => mongoose.disconnect());



const express = require('express');
const path = require('path');
const passport = require('passport');
const cors = require('cors');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

const userRouter = require('./routes/user.routes');

const artistRouter = require('./routes/artist.routes');
const ConcertHallRouter = require('./routes/concerthall.routes');
const styleRouter = require('./routes/style.routes');
const concertRouter = require('./routes/concert.routes');
const ticketRouter = require('./routes/ticket.routes');

require('dotenv').config();

const db = require('./db.js');
db.connect();

require('./passport');

const PORT = process.env.PORT || 5000;

const server = express();

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

server.use(cors({
    origin: ['http://localhost:3000', 'https://thebestrythme.netlify.app'],
    credentials: true,
}));

server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use(express.static(path.join(__dirname, 'public')))

server.enable('trust proxy');

server.use(
    session({
        secret: process.env.SESSION_SECRET,
        resave: false,
        saveUninitialized: false,
        proxy: true,
        cookie: {
            maxAge: 4 * 60 * 60 * 1000,
            httpOnly: false,
            secure: true,
            sameSite: 'none',
        },
        store: new MongoStore({ mongooseConnection: mongoose.connection }),
    })
);

server.use(passport.initialize());

server.use(passport.session());

server.use("/auth", userRouter);
server.use("/artist", artistRouter);
server.use("/concertHall", ConcertHallRouter);
server.use("/style", styleRouter);
server.use("/concert", concertRouter);
server.use("/ticket", ticketRouter);

server.use('*', (req, res, next) => {
    const error = new Error('Ups! Route not found');
    error.status = 404;
    next(error);
})

server.use((err, req, res, next) => {
    console.log(err);
    return res.status(err.status || 500).json(err.message);
});

server.listen(PORT, () => {
    console.log(`Server started on http://localhost:${PORT}`);
});